#add ros deb packages
include ${COREBASE}/meta-qti-ros-oss/meta-ros2-foxy/recipes-core/ubuntu-base/ros2-foxy-packages.inc
include ${COREBASE}/meta-qti-ros-oss/meta-ros1-noetic/recipes-core/ubuntu-base/ros1-noetic-packages.inc

UBUN_PLATFORM_EXTRA_PACKAGES += " ${@bb.utils.contains('DISTRO_FEATURES', 'ros2-foxy', '${ROS2-FOXY-PACKAGES}', '', d)} \
                                  ${@bb.utils.contains('DISTRO_FEATURES', 'ros1-noetic', '${ROS1-NOETIC-PACKAGES}', '', d)} \
                                "
