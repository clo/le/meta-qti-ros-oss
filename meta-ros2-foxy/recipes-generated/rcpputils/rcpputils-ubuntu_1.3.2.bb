inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcpputils = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcpputils
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcpputils/ros-foxy-rcpputils_1.3.2-1focal.20230527.050637_arm64.deb;name=ros-foxy-rcpputils"
SRC_URI[ros-foxy-rcpputils.md5sum] = "c0c5fdceb74dd72c9587afda1acb7fff"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcpputils"
