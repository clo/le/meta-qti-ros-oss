inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-image-transport = "BSD"

# The information of ros2-foxy package: ros-foxy-image-transport
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-image-transport/ros-foxy-image-transport_2.4.0-1focal.20230606.034519_arm64.deb;name=ros-foxy-image-transport"
SRC_URI[ros-foxy-image-transport.md5sum] = "bd9883d2691dbcf59cfbe9fab5ea9159"

DEPENDS = "libtinyxml2-6a console-bridge-vendor message-filters"
DEPENDS += "pluginlib rclcpp sensor-msgs"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-image-transport"
