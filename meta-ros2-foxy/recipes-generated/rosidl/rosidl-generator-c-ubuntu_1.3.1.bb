inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-generator-c = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-generator-c
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-generator-c/ros-foxy-rosidl-generator-c_1.3.1-1focal.20230527.050858_arm64.deb;name=ros-foxy-rosidl-generator-c"
SRC_URI[ros-foxy-rosidl-generator-c.md5sum] = "e1e467ea3c68ef25bb7d0c9f54b014fb"

DEPENDS += "rosidl-typesupport-interface"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-generator-c"
