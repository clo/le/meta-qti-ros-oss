inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-runtime-cpp = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-runtime-cpp
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-runtime-cpp/ros-foxy-rosidl-runtime-cpp_1.3.1-1focal.20230527.045011_arm64.deb;name=ros-foxy-rosidl-runtime-cpp"
SRC_URI[ros-foxy-rosidl-runtime-cpp.md5sum] = "6e9fa5e8d35f4c745cb77a00e54ccfa1"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-runtime-cpp"
