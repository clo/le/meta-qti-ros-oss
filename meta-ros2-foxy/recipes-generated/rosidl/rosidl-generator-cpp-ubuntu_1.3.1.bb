inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-generator-cpp = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-generator-cpp
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-generator-cpp/ros-foxy-rosidl-generator-cpp_1.3.1-1focal.20230527.051037_arm64.deb;name=ros-foxy-rosidl-generator-cpp"
SRC_URI[ros-foxy-rosidl-generator-cpp.md5sum] = "e826816f6cc9fbda411065e9e4fc336e"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-generator-cpp"
