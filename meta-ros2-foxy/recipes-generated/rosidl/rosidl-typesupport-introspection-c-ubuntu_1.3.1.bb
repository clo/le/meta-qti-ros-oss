inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-typesupport-introspection-c = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-typesupport-introspection-c
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-typesupport-introspection-c/ros-foxy-rosidl-typesupport-introspection-c_1.3.1-1focal.20230527.050900_arm64.deb;name=ros-foxy-rosidl-typesupport-introspection-c"
SRC_URI[ros-foxy-rosidl-typesupport-introspection-c.md5sum] = "bbcb6c1c599a983bfc013eb234533477"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-typesupport-introspection-c"
