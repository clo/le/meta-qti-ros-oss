inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-cmake = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-cmake
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-cmake/ros-foxy-rosidl-cmake_1.3.1-1focal.20230527.045122_arm64.deb;name=ros-foxy-rosidl-cmake"
SRC_URI[ros-foxy-rosidl-cmake.md5sum] = "5735cf92457d4c60125e1eee103a9089"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-cmake"
