inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-typesupport-interface = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-typesupport-interface
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-typesupport-interface/ros-foxy-rosidl-typesupport-interface_1.3.1-1focal.20230527.044811_arm64.deb;name=ros-foxy-rosidl-typesupport-interface"
SRC_URI[ros-foxy-rosidl-typesupport-interface.md5sum] = "85db2dc7d33a47a3fdf6a3b8481caa26"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-typesupport-interface"
