inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-adapter = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-adapter
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-adapter/ros-foxy-rosidl-adapter_1.3.1-1focal.20230527.044805_arm64.deb;name=ros-foxy-rosidl-adapter"
SRC_URI[ros-foxy-rosidl-adapter.md5sum] = "78047a7a3e2d147035cf763e401a4ded"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-adapter"

#dependency deb packages
#DEPENDS += "ros-foxy-ros-workspace"
