inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-runtime-c = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-runtime-c
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-runtime-c/ros-foxy-rosidl-runtime-c_1.3.1-1focal.20230527.050613_arm64.deb;name=ros-foxy-rosidl-runtime-c"
SRC_URI[ros-foxy-rosidl-runtime-c.md5sum] = "1fa46e34e1b6d380d1789af47eccbb8d"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-runtime-c"
