inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-tinyxml2-vendor = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-tinyxml2-vendor
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tinyxml2-vendor/ros-foxy-tinyxml2-vendor_0.7.4-1focal.20230527.035815_arm64.deb;name=ros-foxy-tinyxml2-vendor"
SRC_URI[ros-foxy-tinyxml2-vendor.md5sum] = "09584683361c6c29bf2c1b437d92e0ec"

DEPENDS = "libtinyxml2-dev ros-workspace"

# other configs to feed compilation
PKG_${UPN} = "tinyxml2-vendor"
