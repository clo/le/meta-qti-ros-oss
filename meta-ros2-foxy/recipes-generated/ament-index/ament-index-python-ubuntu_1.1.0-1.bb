inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ament-index-python = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ament-index-python
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ament-index-python/ros-foxy-ament-index-python_1.1.0-1focal.20230527.040511_arm64.deb;name=ros-foxy-ament-index-python"
SRC_URI[ros-foxy-ament-index-python.md5sum] = "6d3b8f0138e3e1b30b48216c2de63f55"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ament-index-python"
