inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ament-index-cpp = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ament-index-cpp
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ament-index-cpp/ros-foxy-ament-index-cpp_1.1.0-1focal.20230527.044824_arm64.deb;name=ros-foxy-ament-index-cpp"
SRC_URI[ros-foxy-ament-index-cpp.md5sum] = "fcc03e6c1959e5e28cbff106b41eb64c"

DEPENDS += "ros-workspace"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ament-index-cpp"
