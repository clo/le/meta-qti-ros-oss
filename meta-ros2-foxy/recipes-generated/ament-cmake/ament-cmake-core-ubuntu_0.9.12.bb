inherit upkg_base

SYSROOT_DIRS += "/opt"
BBCLASSEXTEND = "native"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ament-cmake-core = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ament-cmake-core
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ament-cmake-core/ros-foxy-ament-cmake-core_0.9.12-1focal.20230527.032629_arm64.deb;name=ros-foxy-ament-cmake-core"
SRC_URI[ros-foxy-ament-cmake-core.md5sum] = "8cd0757b407c3567b75dd5144a229a4f"

DEPENDS += "python3-catkin-pkg-modules ament-package python3-pkg-resources"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ament-cmake-core"
