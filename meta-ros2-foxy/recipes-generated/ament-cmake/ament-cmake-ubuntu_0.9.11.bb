inherit upkg_base

SYSROOT_DIRS += "/opt"
BBCLASSEXTEND = "native"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ament-cmake = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ament-cmake-core
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ament-cmake/ros-foxy-ament-cmake_0.9.12-1focal.20230527.034003_arm64.deb;name=ros-foxy-ament-cmake"
SRC_URI[ros-foxy-ament-cmake.md5sum] = "07e0e9dbd09612df5ede52bd84ad97bd"

DEPENDS += "ament-cmake-core ament-cmake-core-native"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ament-cmake"
