inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-eigen3-cmake-module = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-eigen3-cmake-module
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-eigen3-cmake-module/ros-foxy-eigen3-cmake-module_0.1.1-1focal.20230527.041126_arm64.deb;name=ros-foxy-eigen3-cmake-module"
SRC_URI[ros-foxy-eigen3-cmake-module.md5sum] = "1d3e7710163ebc6a43a2a158ec7f2667"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-eigen3-cmake-module"
