inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-message-filters = "BSD"

# The information of ros2-foxy package: ros-foxy-message-filters
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-message-filters/ros-foxy-message-filters_3.2.7-1focal.20230606.034003_arm64.deb;name=ros-foxy-message-filters"
SRC_URI[ros-foxy-message-filters.md5sum] = "17584039158ddeac04c248f2e3a485d2"

DEPENDS += "ros-workspace"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-message-filters"
