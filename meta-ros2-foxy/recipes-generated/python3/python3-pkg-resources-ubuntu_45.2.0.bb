inherit upkg_base

SYSROOT_DIRS += "/usr"
BBCLASSEXTEND = "native"
PYTHON_SITEPACKAGES_DIR = "usr/lib/python3.8/site-packages"

LICENSE = "PSF | ZPL"
LICENSE_python3-pkg-resources = "PSF | ZPL"

# The information of ubuntu package: python3-pkg-resources
SRC_URI += "http://ports.ubuntu.com/ubuntu-ports/pool/main/s/setuptools/python3-pkg-resources_45.2.0-1_all.deb;name=python3-pkg-resources"
SRC_URI[python3-pkg-resources.md5sum] = "ef274536b09b191ca3c7e66f235b9b24"

do_install_append() {
	install -d ${D}/usr/lib/python3.8/site-packages
	cp -rf ${DEBOUT}/usr/lib/python3/dist-packages/pkg_resources ${D}/${PYTHON_SITEPACKAGES_DIR}
}

# other configs to feed compilation
PKG_${UPN} = "python3-pkg-resources"
