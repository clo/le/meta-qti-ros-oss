inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "LGPL-2.1+"
LICENSE_ros-foxy-orocos-kdl = "LGPL-2.1+"

# The information of ros2-foxy package: ros-foxy-orocos-kdl
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-orocos-kdl/ros-foxy-orocos-kdl_3.3.5-1focal.20230527.041439_arm64.deb;name=ros-foxy-orocos-kdl"
SRC_URI[ros-foxy-orocos-kdl.md5sum] = "86cb1517cdcc410d573895ee784ee44d"

DEPENDS = "eigen3-cmake-module"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-orocos-kdl"
