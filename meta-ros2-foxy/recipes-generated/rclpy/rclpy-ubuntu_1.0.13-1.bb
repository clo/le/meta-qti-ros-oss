inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rclpy = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rclpy
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rclpy/ros-foxy-rclpy_1.0.13-1focal.20230527.070432_arm64.deb;name=ros-foxy-rclpy"
SRC_URI[ros-foxy-rclpy.md5sum] = "9a180b604bbb9af2d3ffbc0eeb782dfb"

DEPENDS = "ament-index-python builtin-interfaces rcl rcl-action rcl-interfaces"
DEPENDS += "rcl-yaml-param-parser rmw-implementation rosgraph-msgs rpyutils unique-identifier-msgs"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rclpy"
