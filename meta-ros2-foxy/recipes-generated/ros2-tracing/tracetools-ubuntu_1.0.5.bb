inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-tracetools = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-tracetools
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tracetools/ros-foxy-tracetools_1.0.5-2focal.20230527.050452_arm64.deb;name=ros-foxy-tracetools"
SRC_URI[ros-foxy-tracetools.md5sum] = "3fb520d3ecaa6185ea590ddc7fe4e905"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tracetools"
