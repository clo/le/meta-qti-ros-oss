inherit upkg_base
inherit python3native

SYSROOT_DIRS += "/opt"

LICENSE = "BSD-3-Clause"
LICENSE_python3-catkin-pkg-modules = "BSD-3-Clause"

BBCLASSEXTEND = "native"

# The information of ros2-foxy package: python3-catkin-pkg-modules
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/p/python3-catkin-pkg-modules/python3-catkin-pkg-modules_0.4.24-1_all.deb;name=python3-catkin-pkg-modules"
SRC_URI[python3-catkin-pkg-modules.md5sum] = "4972e51cacef257f9885cdc28e87918d"

do_install_append() {
	install -d ${D}${PYTHON_SITEPACKAGES_DIR}
#    install -d ${D}/usr/lib/

    cp -rf ${DEBOUT}/usr/lib/python3/dist-packages/catkin_pkg ${D}${PYTHON_SITEPACKAGES_DIR}
    cp -rf ${DEBOUT}/usr/lib/python3/dist-packages/catkin_pkg_modules-0.4.*.egg-info ${D}${PYTHON_SITEPACKAGES_DIR}
#    cp -rf ${DEBOUT}/../recipe-sysroot/usr/lib/aarch64-linux-gnu/libspdlog.so.* ${D}/usr/lib/
}

# other configs to feed compilation
PKG_${UPN} = "python3-catkin-pkg-modules"
