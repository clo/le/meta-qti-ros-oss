inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "MIT & Apache-2.0"
LICENSE_ros-foxy-libyaml-vendor = "MIT & Apache-2.0"

# The information of ros2-foxy package: ros-foxy-libyaml-vendor
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-libyaml-vendor/ros-foxy-libyaml-vendor_1.0.4-1focal.20230527.051235_arm64.deb;name=ros-foxy-libyaml-vendor"
SRC_URI[ros-foxy-libyaml-vendor.md5sum] = "e3dd97902320453e73c97f372f9e6b5b"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-libyaml-vendor"
