inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-unique-identifier-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-unique-identifier-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-unique-identifier-msgs/ros-foxy-unique-identifier-msgs_2.1.3-1focal.20230527.053240_arm64.deb;name=ros-foxy-unique-identifier-msgs"
SRC_URI[ros-foxy-unique-identifier-msgs.md5sum] = "3b02360b6d961af5c7eff835a3e3f8eb"

DEPENDS = "fastcdr"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-unique-identifier-msgs"
