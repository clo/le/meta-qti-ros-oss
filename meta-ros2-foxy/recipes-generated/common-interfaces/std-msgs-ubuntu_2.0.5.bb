inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-std-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-std-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-std-msgs/ros-foxy-std-msgs_2.0.5-1focal.20230527.053140_arm64.deb;name=ros-foxy-std-msgs"
SRC_URI[ros-foxy-std-msgs.md5sum] = "9eeeb01ed006120df8a19617580813bc"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-std-msgs"
