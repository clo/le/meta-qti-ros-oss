inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-sensor-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-sensor-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-sensor-msgs/ros-foxy-sensor-msgs_2.0.5-1focal.20230527.055054_arm64.deb;name=ros-foxy-sensor-msgs"
SRC_URI[ros-foxy-sensor-msgs.md5sum] = "dab905e5dd9cb1d7890a1261443bcec8"

DEPENDS += "builtin-interfaces geometry-msgs std-msgs" 

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-sensor-msgs"
