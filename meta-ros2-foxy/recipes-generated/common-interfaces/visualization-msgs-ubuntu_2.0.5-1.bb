inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-visualization-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-visualization-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-visualization-msgs/ros-foxy-visualization-msgs_2.0.5-1focal.20230527.061156_arm64.deb;name=ros-foxy-visualization-msgs"
SRC_URI[ros-foxy-visualization-msgs.md5sum] = "7eb6de04fbf089d87f880d2b89bbc498"

DEPENDS = "fastcdr builtin-interfaces geometry-msgs std-msgs"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-visualization-msgs"
