inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-geometry-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-geometry-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-geometry-msgs/ros-foxy-geometry-msgs_2.0.5-1focal.20230527.054419_arm64.deb;name=ros-foxy-geometry-msgs"
SRC_URI[ros-foxy-geometry-msgs.md5sum] = "6c3c03a8ba7d9aa5044ffa45e3e65dbe"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-geometry-msgs"
