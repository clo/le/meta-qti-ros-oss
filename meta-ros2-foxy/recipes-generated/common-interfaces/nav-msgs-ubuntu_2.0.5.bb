inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-nav-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-nav-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-nav-msgs/ros-foxy-nav-msgs_2.0.5-1focal.20230527.054845_arm64.deb;name=ros-foxy-nav-msgs"
SRC_URI[ros-foxy-nav-msgs.md5sum] = "9c511fcb63c108bbd7c0fc6de34c3ca3"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-nav-msgs"
