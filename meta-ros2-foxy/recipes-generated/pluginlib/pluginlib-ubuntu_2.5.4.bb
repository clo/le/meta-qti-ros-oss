inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-pluginlib = "BSD"

# The information of ros2-foxy package: ros-foxy-pluginlib
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-pluginlib/ros-foxy-pluginlib_2.5.4-1focal.20230527.051434_arm64.deb;name=ros-foxy-pluginlib"
SRC_URI[ros-foxy-pluginlib.md5sum] = "208a84b8d342516e785962fb9fc69e58"

DEPENDS = "ament-index-cpp class-loader"
DEPENDS += "rcpputils rcutils tinyxml2-vendor"

# other configs to feed compilation
PKG_${UPN} = "pluginlib"
