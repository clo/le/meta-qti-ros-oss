inherit upkg_base

SYSROOT_DIRS += "/usr"

LICENSE = "Zlib"
LICENSE_libtinyxml2-dev = "Zlib"

# The information of ubuntu package: libtinyxml2-dev
SRC_URI += "http://ports.ubuntu.com/ubuntu-ports/pool/universe/t/tinyxml2/libtinyxml2-dev_7.0.0+dfsg-1build1_arm64.deb;name=libtinyxml2-dev"
SRC_URI[libtinyxml2-dev.md5sum] = "c79d213f414dd22fae513b93c05140af"

# other configs to feed compilation
PKG_${UPN} = "libtinyxml2-dev"
