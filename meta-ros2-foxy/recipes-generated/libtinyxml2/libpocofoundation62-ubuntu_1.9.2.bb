inherit upkg_base

SYSROOT_DIRS += "/usr"

LICENSE = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"
LICENSE_libpocofoundation62 = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"

# The information of ubuntu package: libpocofoundation62
SRC_URI += "http://ports.ubuntu.com/ubuntu-ports/pool/universe/p/poco/libpocofoundation62_1.9.2-3ubuntu3_arm64.deb;name=libpocofoundation62"
SRC_URI[libpocofoundation62.md5sum] = "5edbb68b4e5673333d6d05dc43851588"

# other configs to feed compilation
PKG_${UPN} = "libpocofoundation62"
