inherit upkg_base

SYSROOT_DIRS += "/usr"
#BBCLASSEXTEND = "native"

LICENSE = "Zlib"
LICENSE_libtinyxml2-6a = "Zlib"

# The information of ubuntu package: libtinyxml2-6a
SRC_URI += "http://ports.ubuntu.com/ubuntu-ports/pool/universe/t/tinyxml2/libtinyxml2-6a_7.0.0+dfsg-1build1_arm64.deb;name=libtinyxml2-6a"
SRC_URI[libtinyxml2-6a.md5sum] = "2e94e151977cf13183d734fd3ca454d2"

# other configs to feed compilation
PKG_${UPN} = "libtinyxml2-6a"
