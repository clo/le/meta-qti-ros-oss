inherit upkg_base

SYSROOT_DIRS += "/usr"

LICENSE = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"
LICENSE_libpoco-dev = "BSL-1.0 & BSD-3-clause & MIT & PD & Zlib"

# The information of ubuntu package: libpoco-dev
SRC_URI += "http://ports.ubuntu.com/ubuntu-ports/pool/universe/p/poco/libpoco-dev_1.9.2-3ubuntu3_arm64.deb;name=libpoco-dev"
SRC_URI[libpoco-dev.md5sum] = "a29e46c931075812a2d8e5fd78e6b730"

DEPENDS = "libpocofoundation62"

# other configs to feed compilation
PKG_${UPN} = "libpoco-dev"
