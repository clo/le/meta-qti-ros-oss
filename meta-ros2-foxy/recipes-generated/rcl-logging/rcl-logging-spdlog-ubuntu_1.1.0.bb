inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl-logging-spdlog = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl-logging-spdlog
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl-logging-spdlog/ros-foxy-rcl-logging-spdlog_1.1.0-1focal.20230527.051207_arm64.deb;name=ros-foxy-rcl-logging-spdlog"
SRC_URI[ros-foxy-rcl-logging-spdlog.md5sum] = "81b502c1a416b4551b31445494c66eee"

DEPENDS = "spdlog"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl-logging-spdlog"
