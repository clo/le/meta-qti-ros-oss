inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl-logging-noop = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl-logging-noop
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl-logging-noop/ros-foxy-rcl-logging-noop_1.1.0-1focal.20230527.050636_arm64.deb;name=ros-foxy-rcl-logging-noop"
SRC_URI[ros-foxy-rcl-logging-noop.md5sum] = "3fd45f79f3ec47b34aa5e0b39b24bf45"

DEPENDS += "rcutils ros-workspace"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl-logging-noop"
