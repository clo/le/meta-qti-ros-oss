inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rmw-implementation = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rmw-implementation
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rmw-implementation/ros-foxy-rmw-implementation_1.0.3-1focal.20230527.064741_arm64.deb;name=ros-foxy-rmw-implementation"
SRC_URI[ros-foxy-rmw-implementation.md5sum] = "e95c3cb3cdf79847f5c6fbb6caee7b5e"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rmw-implementation"
