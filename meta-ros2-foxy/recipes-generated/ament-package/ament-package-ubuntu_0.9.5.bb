inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ament-package = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ament-package
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ament-package/ros-foxy-ament-package_0.9.5-1focal.20210422.231141_arm64.deb;name=ros-foxy-ament-package"
SRC_URI[ros-foxy-ament-package.md5sum] = "be890fb00c3f059182b93929a8ca722f"

BBCLASSEXTEND = "native"

do_install_append(){
    cp -rf  ${DEBOUT}/opt/ros/foxy/lib/python3.8/site-packages ${D}${PYTHON_SITEPACKAGES_DIR}
    cp -rf  ${DEBOUT}/opt/ros/foxy/share/ ${D}${PYTHON_SITEPACKAGES_DIR}
}

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ament-package"
