inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rpyutils = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rpyutils
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rpyutils/ros-foxy-rpyutils_0.2.0-1focal.20230527.040658_arm64.deb;name=ros-foxy-rpyutils"
SRC_URI[ros-foxy-rpyutils.md5sum] = "dab76467f92212e767d3a08bdc8b3d75"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rpyutils"
