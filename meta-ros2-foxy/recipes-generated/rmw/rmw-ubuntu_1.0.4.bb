inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rmw = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rmw
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rmw/ros-foxy-rmw_1.0.4-1focal.20230527.050902_arm64.deb;name=ros-foxy-rmw"
SRC_URI[ros-foxy-rmw.md5sum] = "2e278b6c29be7349b7a1b7f819680994"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rmw"
