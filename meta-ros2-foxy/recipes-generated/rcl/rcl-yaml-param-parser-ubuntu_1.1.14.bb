inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl-yaml-param-parser = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl-yaml-param-parser
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl-yaml-param-parser/ros-foxy-rcl-yaml-param-parser_1.1.14-1focal.20230527.051509_arm64.deb;name=ros-foxy-rcl-yaml-param-parser"
SRC_URI[ros-foxy-rcl-yaml-param-parser.md5sum] = "4520c8c217e417ca993472a070b1bb8d"

DEPENDS = "libyaml-vendor"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl-yaml-param-parser"
