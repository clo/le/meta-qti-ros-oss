inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl/ros-foxy-rcl_1.1.14-1focal.20230527.064949_arm64.deb;name=ros-foxy-rcl"
SRC_URI[ros-foxy-rcl.md5sum] = "8e3ee5a15c67ed788cc06aecc15407b0"

DEPENDS = "rcl-interfaces rcl-logging-spdlog rcl-yaml-param-parser"
DEPENDS += "rmw rmw-implementation rosidl-runtime-c tracetools"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl"
