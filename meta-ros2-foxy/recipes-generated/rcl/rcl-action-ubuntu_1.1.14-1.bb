inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl-action = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl-action
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl-action/ros-foxy-rcl-action_1.1.14-1focal.20230527.065734_arm64.deb;name=ros-foxy-rcl-action"
SRC_URI[ros-foxy-rcl-action.md5sum] = "604e772696714b3e3386cc486fb835dd"

DEPENDS = "action-msgs rcl rcutils rmw rosidl-runtime-c"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl-action"
