inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-cv-bridge = "BSD"

# The information of ros2-foxy package: ros-foxy-cv-bridge
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-cv-bridge/ros-foxy-cv-bridge_3.0.7-1focal.20230527.055710_arm64.deb;name=ros-foxy-cv-bridge"
SRC_URI[ros-foxy-cv-bridge.md5sum] = "3d2cbd25b777a0be3045ec3da6efd8f3"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-cv-bridge"
