inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcutils = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcutils
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcutils/ros-foxy-rcutils_1.1.5-1focal.20230527.050146_arm64.deb;name=ros-foxy-rcutils"
SRC_URI[ros-foxy-rcutils.md5sum] = "59401d41b940c2faf178ec67c79bef45"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcutils"
