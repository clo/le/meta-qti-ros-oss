inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-ros-workspace = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-ros-workspace
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-ros-workspace/ros-foxy-ros-workspace_1.0.2-1focal.20230527.033103_arm64.deb;name=ros-foxy-ros-workspace"
SRC_URI[ros-foxy-ros-workspace.md5sum] = "5f2fb20ba2bdbbbe0115c5c7e6e1490e"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-ros-workspace"
