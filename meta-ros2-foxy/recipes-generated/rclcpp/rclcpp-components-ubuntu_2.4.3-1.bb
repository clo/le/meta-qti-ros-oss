inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rclcpp-components = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rclcpp-components
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rclcpp-components/ros-foxy-rclcpp-components_2.4.3-1focal.20230527.082118_arm64.deb;name=ros-foxy-rclcpp-components"
SRC_URI[ros-foxy-rclcpp-components.md5sum] = "6fe57950f95537d5d6c640e88b4c5543"

DEPENDS = "console-bridge-vendor ament-index-cpp class-loader composition-interfaces rclcpp"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rclcpp-components"
