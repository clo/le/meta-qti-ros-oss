inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rclcpp-action = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rclcpp-action
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rclcpp-action/ros-foxy-rclcpp-action_2.4.3-1focal.20230527.081314_arm64.deb;name=ros-foxy-rclcpp-action"
SRC_URI[ros-foxy-rclcpp-action.md5sum] = "f5ad57a29b01aa006b2031d79f3fa066"

DEPENDS = "action-msgs ament-cmake rcl-action rclcpp rosidl-runtime-c"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rclcpp-action"
