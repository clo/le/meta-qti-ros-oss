inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rclcpp = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rclcpp
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rclcpp/ros-foxy-rclcpp_2.4.3-1focal.20230527.072753_arm64.deb;name=ros-foxy-rclcpp"
SRC_URI[ros-foxy-rclcpp.md5sum] = "77c6ba3a5c63eecf3c7c937576a10b92"

DEPENDS = "builtin-interfaces libstatistics-collector rcl rcl-interfaces rcl-yaml-param-parser"
DEPENDS += "rcpputils rcutils rmw rosgraph-msgs rosidl-typesupport-c rosidl-typesupport-cpp"
DEPENDS += "rosidl-runtime-cpp statistics-msgs tracetools"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rclcpp"
