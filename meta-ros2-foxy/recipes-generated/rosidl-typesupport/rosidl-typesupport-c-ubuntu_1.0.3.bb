inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-typesupport-c = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-typesupport-c
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-typesupport-c/ros-foxy-rosidl-typesupport-c_1.0.3-1focal.20230527.051637_arm64.deb;name=ros-foxy-rosidl-typesupport-c"
SRC_URI[ros-foxy-rosidl-typesupport-c.md5sum] = "deb02d078d54f8c15f0c4e5d78d0647e"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-typesupport-c"
