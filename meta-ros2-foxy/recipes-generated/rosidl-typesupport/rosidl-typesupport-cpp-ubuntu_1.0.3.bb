inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rosidl-typesupport-cpp = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosidl-typesupport-cpp
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosidl-typesupport-cpp/ros-foxy-rosidl-typesupport-cpp_1.0.3-1focal.20230527.051851_arm64.deb;name=ros-foxy-rosidl-typesupport-cpp"
SRC_URI[ros-foxy-rosidl-typesupport-cpp.md5sum] = "769791f98e269a8ac5fce3261414ae10"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rosidl-typesupport-cpp"
