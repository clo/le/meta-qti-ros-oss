inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-action-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-action-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-action-msgs/ros-foxy-action-msgs_1.0.0-1focal.20230527.061324_arm64.deb;name=ros-foxy-action-msgs"
SRC_URI[ros-foxy-action-msgs.md5sum] = "faa13cbe14215de04f17406b2a8abb81"

DEPENDS = "fastcdr builtin-interfaces unique-identifier-msgs"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-action-msgs"
