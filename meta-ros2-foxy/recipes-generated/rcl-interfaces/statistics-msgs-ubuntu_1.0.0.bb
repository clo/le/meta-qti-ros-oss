inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-statistics-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-statistics-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-statistics-msgs/ros-foxy-statistics-msgs_1.0.0-1focal.20230527.053108_arm64.deb;name=ros-foxy-statistics-msgs"
SRC_URI[ros-foxy-statistics-msgs.md5sum] = "d4c5dceccf375d2a21e2cf818dd17419"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-statistics-msgs"
