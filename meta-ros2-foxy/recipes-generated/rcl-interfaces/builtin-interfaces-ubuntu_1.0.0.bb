inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-builtin-interfaces = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-builtin-interfaces
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-builtin-interfaces/ros-foxy-builtin-interfaces_1.0.0-1focal.20230527.052241_arm64.deb;name=ros-foxy-builtin-interfaces"
SRC_URI[ros-foxy-builtin-interfaces.md5sum] = "a2009478ca5d4d7470ccd271cbff98b6"

DEPENDS = "ros-workspace"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-builtin-interfaces"
