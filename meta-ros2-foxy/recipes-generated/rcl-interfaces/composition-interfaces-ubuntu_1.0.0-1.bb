inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-composition-interfaces = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-composition-interfaces
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-composition-interfaces/ros-foxy-composition-interfaces_1.0.0-1focal.20230527.052959_arm64.deb;name=ros-foxy-composition-interfaces"
SRC_URI[ros-foxy-composition-interfaces.md5sum] = "41581ee532b98374a8ec5bd630c59085"

DEPENDS = "fastcdr rcl-interfaces"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-composition-interfaces"
