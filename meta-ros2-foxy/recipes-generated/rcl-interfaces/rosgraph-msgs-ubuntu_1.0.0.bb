inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_rosgraph-msgs = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rosgraph-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rosgraph-msgs/ros-foxy-rosgraph-msgs_1.0.0-1focal.20230527.053100_arm64.deb;name=ros-foxy-rosgraph-msgs"
SRC_URI[ros-foxy-rosgraph-msgs.md5sum] = "f55372d3678856aa69fa123b1618878e"

# other configs to feed compilation
PKG_${UPN} = "rosgraph-msgs"
