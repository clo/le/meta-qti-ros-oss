inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-rcl-interfaces = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-rcl-interfaces
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-rcl-interfaces/ros-foxy-rcl-interfaces_1.0.0-1focal.20230527.052551_arm64.deb;name=ros-foxy-rcl-interfaces"
SRC_URI[ros-foxy-rcl-interfaces.md5sum] = "cfd397020e75c25343291b9787f3234f"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-rcl-interfaces"
