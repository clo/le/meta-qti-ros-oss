inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-class-loader = "BSD"

# The information of ros2-foxy package: ros-foxy-class-loader
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-class-loader/ros-foxy-class-loader_2.0.3-1focal.20230527.051208_arm64.deb;name=ros-foxy-class-loader"
SRC_URI[ros-foxy-class-loader.md5sum] = "dc6e60589434e7d14743ec249316dadf"

DEPENDS += "console-bridge-vendor "

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-class-loader"
