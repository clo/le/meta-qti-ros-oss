inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-fastcdr = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-fastcdr
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-fastcdr/ros-foxy-fastcdr_1.0.13-1focal.20230527.033526_arm64.deb;name=ros-foxy-fastcdr"
SRC_URI[ros-foxy-fastcdr.md5sum] = "1948e81fe09b58c648d6f1441b50f384"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-fastcdr"
