inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-tf2-msgs = "BSD"

# The information of ros2-foxy package: ros-foxy-tf2-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tf2-msgs/ros-foxy-tf2-msgs_0.13.14-1focal.20230527.062217_arm64.deb;name=ros-foxy-tf2-msgs"
SRC_URI[ros-foxy-tf2-msgs.md5sum] = "e4db4959e5600916cf9a42cbb3ee301c"

DEPENDS = "fastcdr action-msgs builtin-interfaces geometry-msgs"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tf2-msgs"
