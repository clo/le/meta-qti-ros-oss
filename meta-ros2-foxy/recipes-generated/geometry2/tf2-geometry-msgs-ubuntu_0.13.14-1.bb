inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-tf2-geometry-msgs = "BSD"

# The information of ros2-foxy package: ros-foxy-tf2-geometry-msgs
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tf2-geometry-msgs/ros-foxy-tf2-geometry-msgs_0.13.14-1focal.20230606.040417_arm64.deb;name=ros-foxy-tf2-geometry-msgs"
SRC_URI[ros-foxy-tf2-geometry-msgs.md5sum] = "e0e2cfe8f4fddc41b25e4bf809afd5d8"

DEPENDS = "geometry-msgs orocos-kdl tf2 tf2-ros"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tf2-geometry-msgs"
