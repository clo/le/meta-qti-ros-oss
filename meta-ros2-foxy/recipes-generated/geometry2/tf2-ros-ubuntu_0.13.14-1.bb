inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-tf2-ros = "BSD"

# The information of ros2-foxy package: ros-foxy-tf2-ros
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tf2-ros/ros-foxy-tf2-ros_0.13.14-1focal.20230606.034740_arm64.deb;name=ros-foxy-tf2-ros"
SRC_URI[ros-foxy-tf2-ros.md5sum] = "1b1259354f8ca72027918c4ffa58e075"

DEPENDS = "console-bridge-vendor builtin-interfaces geometry-msgs message-filters rcl-interfaces"
DEPENDS += "rclcpp rclcpp-action rclcpp-components rclpy std-msgs tf2"
DEPENDS += "tf2-msgs tf2-py"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tf2-ros"
