inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-tf2 = "BSD"

# The information of ros2-foxy package: ros-foxy-tf2
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tf2/ros-foxy-tf2_0.13.14-1focal.20230527.060805_arm64.deb;name=ros-foxy-tf2"
SRC_URI[ros-foxy-tf2.md5sum] = "2b3b8a3ed265604fa2de9ef251e6bd50"

DEPENDS = "console-bridge-vendor geometry-msgs rcutils"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tf2"
