inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "BSD"
LICENSE_ros-foxy-tf2-py = "BSD"

# The information of ros2-foxy package: ros-foxy-tf2-py
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-tf2-py/ros-foxy-tf2-py_0.13.14-1focal.20230527.071941_arm64.deb;name=ros-foxy-tf2-py"
SRC_URI[ros-foxy-tf2-py.md5sum] = "589bb2fa1d28c90d405bd6fcb2b087f1"

DEPENDS = "builtin-interfaces geometry-msgs rclpy rpyutils tf2"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-tf2-py"
