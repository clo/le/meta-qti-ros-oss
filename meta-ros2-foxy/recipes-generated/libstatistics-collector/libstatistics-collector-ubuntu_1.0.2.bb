inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0"
LICENSE_ros-foxy-libstatistics-collector = "Apache-2.0"

# The information of ros2-foxy package: ros-foxy-libstatistics-collector
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-libstatistics-collector/ros-foxy-libstatistics-collector_1.0.2-1focal.20230527.065734_arm64.deb;name=ros-foxy-libstatistics-collector"
SRC_URI[ros-foxy-libstatistics-collector.md5sum] = "095edabda12caaa9c9223238a4407a1d"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-libstatistics-collector"
