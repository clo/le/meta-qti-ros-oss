inherit upkg_base

SYSROOT_DIRS += "/opt"

LICENSE = "Apache-2.0 & BSD"
LICENSE_ros-foxy-console-bridge-vendor = "Apache-2.0 & BSD"

# The information of ros2-foxy package: ros-foxy-console-bridge-vendor
SRC_URI += "http://packages.ros.org/ros2/ubuntu/pool/main/r/ros-foxy-console-bridge-vendor/ros-foxy-console-bridge-vendor_1.2.4-1focal.20230527.045200_arm64.deb;name=ros-foxy-console-bridge-vendor"
SRC_URI[ros-foxy-console-bridge-vendor.md5sum] = "e89c7993d0d439053b32f7e7a6badbf9"

# other configs to feed compilation
PKG_${UPN} = "ros-foxy-console-bridge-vendor"
